#===== Project setup =====
  1. git clone https://dbo_soft@bitbucket.org/dbo_soft/test-task.git
  2. npm install
  3. node server.js
  4. ng serve --open

#===== Endpoints =====
  http://localhost:3004/books
  http://localhost:3004/formats
  http://localhost:3004/countries
  http://localhost:3004/cities
  http://localhost:3004/companies

#===== Request Authentication header =====
  For retrieving data every request should be supplied with 
``"x-auth-token: bad18eba1ff45jk7858b8ae88a77fa30"`` header.


#===== Step 1 =====
  'Main' - page:
  1) Output list of available books on main page.
  2) List items should contain links to separate page (similar to 'Add New Book') and pre-populate the form.
#===== Step 2 =====
  'Main' - page:
  Add input field and implement the ability to sort books by
  1) author
  2) title
  3) page count
  4) country
#===== Step 3 =====
  'Add New Book' - page:
  1) Add form validation and display errors under invalid fields.
  2) All fields are required.
  3) Format, Country, City, Company options should be loaded from server.
  4) Save form data to server.

#===== Step 4 =====
  'Search' - page:
  1) Format options should be loaded from server.
  2) Form fields are filters and are optional, although they should still be validated where needed.
  3) List of filtered books should have the same look as the list on the 'Main' page.
  4) Form fields state changes should be immediately reflected in url GET parameters dynamically, e.g.:
    Form content:
      Author: Miguel De Cervantes
      Format: Paperback

  Corresponding url: "http://localhost:4200/search?author=Miguel%20De%20Cervantes&formatId=1"

  It should be possible to copy retrieved url, enter in a new browser tab and get the same form state.
  
